const express = require('express');
var app = express();

const PORT = 7000;

app.get('/', (req,res,next) => {
    res.send("Hi, This is a dockerized express app sending a random number less than 100 for each request " + Math.round(Math.random() * 100));
})


app.listen(PORT, () => console.log(`Server Started in Port ${PORT}`))