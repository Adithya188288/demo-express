FROM node:latest
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 7000
ENTRYPOINT ["node", "server.js"]